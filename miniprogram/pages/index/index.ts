Page({  
  data: {  
    questionInput: '', // 用户输入的问题  
    previousMessages: [] // 之前的聊天内容  
  },  
    
  inputQuestionChange: function(e) {  
    this.setData({  
      questionInput: e.detail.value  
    });  
  },  
    
  submitQuestion: function() {  
    if (this.data.questionInput) {  
      // 创建一个新的聊天条目，类型为'question'  
      const question = {  
        type: 'question',  
        text: this.data.questionInput  
      };  
        
      // 发送网络请求到后端接口  
      wx.request({  
        url: 'https://eco-sit-api.smartchina.com.cn/ecom/3rd-integration/fsp/check/health', // 替换为您的后端接口地址  
        method: 'GET', // 或者GET，取决于您的后端API要求  
        data: {  
          question: this.data.questionInput // 根据您的后端API需求传递参数  
        },  
        success: (res) => {  
          console.log(res);
          console.log(res.data);
          // 假设后端返回了一个包含答案的对象  
          if (res.data && res.data) {  
            const answer = res.data; // 获取后端返回的答案  
  
            // 创建一个新的聊天条目，类型为'answer'  
            const newAnswer = {  
              type: 'answer',  
              text: answer  
            };  
  
            // 更新聊天上下文  
            const updatedMessages = [...this.data.previousMessages, question, newAnswer];  
            this.setData({  
              previousMessages: updatedMessages,  
              questionInput: '' // 清空输入框  
            });  
          } else {  
            // 处理错误情况，比如显示一个错误提示  
            wx.showToast({  
              title: '获取答案失败',  
              icon: 'none'  
            });  
          }  
        },  
        fail: (err) => {  
          // 处理网络请求失败的情况  
          console.error('请求失败：', err);  
          wx.showToast({  
            title: '网络错误',  
            icon: 'none'  
          });  
        }  
      });  
        
      // // 更新聊天上下文  
      // const updatedMessages = [...this.data.previousMessages, question, newAnswer];  
      // this.setData({  
      //   previousMessages: updatedMessages,  
      //   questionInput: '' // 清空输入框  
      // });  
        
      // 在实际项目中，你会发送一个网络请求到后端并处理响应  
    }  
  }  
    
  // ... 其他代码 ...  
});